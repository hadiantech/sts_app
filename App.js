import React, {Component} from 'react';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import {Icon} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

import getTheme from './native-base-theme/components';
import theme from './native-base-theme/variables/material';

//Screen
import HomeScreen from './src/components/pages/home';
import CoursesScreen from './src/components/pages/courses';
import CourseSubScreen from './src/components/pages/course_sub';
import CourseDateScreen from './src/components/pages/course_date';
import CourseDateDetailScreen from './src/components/pages/course_date_detail';
import AboutScreen from './src/components/pages/about';
import SearchScreen from './src/components/pages/search';
import CourseRegisterScreen from './src/components/pages/course_register';
import CourseRegisterPartScreen from './src/components/pages/course_register_part';
import QRResultScreen from './src/components/pages/qrresult';

import QrScreen from './src/components/pages/qr_scan';



//disable warning
console.ignoredYellowBox = ['Warning'];



//Route stack untuk Home pages
const HomeStack = createStackNavigator({
  Home: HomeScreen,
  Courses: CoursesScreen,
  CourseSub: CourseSubScreen,
  CourseDate: CourseDateScreen,
  CourseDateDetail: CourseDateDetailScreen,
  CourseRegister: CourseRegisterScreen,
  CourseRegisterPart: CourseRegisterPartScreen
});

const SearchStack = createStackNavigator({
  Search: SearchScreen
});

const QrStack = createStackNavigator({
  QRScan: QrScreen,
  QRResult: QRResultScreen
});

//todo this tonight
// const HomeStack = createStackNavigator({
//   Home: { screen: HomeScreen },
//   Details: { screen: DetailsScreen },
// });



export default createBottomTabNavigator(

  {
    Home: { screen: HomeStack },
    Scan: { screen: QrStack },
    Search: { screen: SearchStack },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home${focused ? '' : '-outline'}`;
        } else if (routeName === 'Search') {
          iconName = `ios-search${focused ? '' : '-outline'}`;
        } else if (routeName === 'Scan') {
          iconName = `ios-qr-scanner${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
  }
);

// export default class App extends React.Component {
//   render() {
//     return <RootStack />;
//   }
// }
