import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import { Container, Button, Text, Right, Header, Content, Card, CardItem, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';

const Modal = () => {
  return (
    <Container>
        <Header>
            <Right>
                <Button primary onPress={() => Actions.pop()} >
                    <Text>Close</Text>
                </Button>
            </Right>
        </Header>
        <Content padder>
            <Card>
                <CardItem header>
                    <Text>Select Course</Text>
                </CardItem>

                <CardItem button onPress={() => Actions.courseDate()}>
                    <Text style={{flex: 1}}>Couses On Green 5S Sirim</Text>
                    <Right style={{flex: 0}}>
                        <Icon name="arrow-forward" />
                    </Right>
                </CardItem>

                <CardItem button onPress={() => Actions.courseDate()}>
                    <Text style={{flex: 1}}>Kursus 5S Hijau Sirim</Text>
                    <Right style={{flex: 0}}>
                        <Icon name="arrow-forward" />
                    </Right>
                </CardItem>

            </Card>
        </Content>
    </Container>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default Modal;