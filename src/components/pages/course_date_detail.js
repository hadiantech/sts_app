import React, {Component} from 'react';
import {Content, Text } from 'native-base';
import { StyleSheet, FlatList, View, TouchableOpacity } from 'react-native';


class CourseDateDetailScreen extends React.Component {
    constructor(props){
        super(props);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Select Date & Time',
            headerStyle:{
                backgroundColor: '#2C3B8D',
            },
            headerTitleStyle:{
                fontWeight: 'bold',
                color: 'white',
                alignSelf: 'center',
                fontSize: 20,
                flex: 2,
            }
        };
    };

    state = {date: 'test'}
        updateDate = (date) => {
            this.setState({ date: date })
    }

    render() {
        console.disableYellowBox = true;
        const { navigation } = this.props;
        const screenName = navigation.getParam('screenName', 'NO-ID');

        return (
        <Content>
            <View>

                <FlatList
                    data={this.props.navigation.state.params.listDate}
                    renderItem={({item}) =>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CourseDate',{
                            venueSelected: item.venue,
                            dateStartSelected: item.datestart,
                            durationSelected: item.duration,
                            dateid: item.dateid
                            })}>
                            <Text style={styles.item} >
                            {item.datestart} - {item.dateend} ({item.venue})
                        </Text>
                    </TouchableOpacity>
                }/>

            </View>

        </Content>


        );
    }
}

const styles = StyleSheet.create({
    title:{
        marginBottom:7,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
    },
    detail:{
        fontSize:20,
    },
    item:{
        backgroundColor: 'white',
        borderWidth:0.5,
        borderColor: '#E1E1E1',
        margin: 0,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop:20,
        paddingBottom:20,
    },
});

export default CourseDateDetailScreen;
