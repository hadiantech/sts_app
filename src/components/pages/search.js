import React, {Component} from 'react';

import { View } from 'react-native';
import {Content, Container, Item, Input, Label, Picker, Icon, Button, Text } from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';

class SearchScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: undefined,
            courses: undefined,
            year: undefined,
            visible: true,
            dataSource: [],
            dataSourceCourses: []
        };
    }

    componentDidMount(){
        return fetch('https://www.sirimsense.com/sstsapi/category.php?catstatusid=01')
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            this.setState({
                visible: false,
                dataSource: responseJson,
            }, function(){
                
            });
    
          })
          .catch((error) =>{
            console.error(error);
          });
    }

    static navigationOptions = {
        title: 'Search',
        headerStyle:{
            backgroundColor: '#2C3B8D',
        },
        headerTitleStyle:{
            fontWeight: 'bold',
            color: 'white',
            alignSelf: 'center',
            fontSize: 20,
            flex: 2,
        }
    };

    onCatChange(value) {
        this.setState({
            visible: true
        });

        return fetch('https://www.sirimsense.com/sstsapi/course.php?catstatusid=01&categoryid='+value+'&year1='+this.state.year)
            .then((response) => response.json())
            .then((responseJson) => {
            console.log(responseJson);
            this.setState({
                visible: false,
                dataSourceCourses: responseJson,
                categories: value
            }, function(){
                
            });

            })
            .catch((error) =>{
                console.error(error);
            });
    }

    onCoursesChange(value) {
        this.setState({
            courses: value
        });
    }

    onYearChange(value) {
        this.setState({
            year: value
        });
    }

    render() {
        return (

        <Container style = {{ backgroundColor: '#E1E1E1' }}>
            <Content style = {{ margin: 10 }}>
                
                <Spinner visible={this.state.visible} />
                
                <View style={{marginTop: 22, padding:15}}>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            selectedValue={this.state.year}
                            onValueChange={this.onYearChange.bind(this)}>
                            <Picker.Item label="Please Select Year" value="" />
                            <Picker.Item label="2018" value="2018" />
                            <Picker.Item label="2019" value="2019" />
                        </Picker>
                    </Item>
                    
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            selectedValue={this.state.categories}
                            onValueChange={this.onCatChange.bind(this)}>
                                <Picker.Item label="Please Select Category" value="" />
                                { this.state.dataSource.map((item, key)=>(
                                <Picker.Item label={item.catdesc} value={item.catid} key={key} />
                                ))}
                        </Picker>
                    </Item>

                    <Item picker>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name="arrow-down" />}
                            style={{ width: undefined }}
                            selectedValue={this.state.courses}
                            onValueChange={this.onCoursesChange.bind(this)}>
                                <Picker.Item label="Please Select Course" value="" />
                                { this.state.dataSourceCourses.map((item, key)=>(
                                <Picker.Item label={item.coursedesc} value={item.courseid} key={key} />
                                ))}
                        </Picker>
                    </Item>

                    <Item floatingLabel>
                        <Label>Title</Label>
                        <Input />
                    </Item>
                </View>

                <View style = {{ flex:1, padding:15 }}>
                    <Button block primary onPress={() => this.props.navigation.push('CourseDate')} >
                        <Text>Search</Text>
                    </Button>
                    
                    <Button bordered block danger>
                        <Text>Reset</Text>
                    </Button>

                </View>


            </Content>
        </Container>
        );
    }
}

export default SearchScreen;