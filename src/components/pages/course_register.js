import React, {Component} from 'react';
import { Container, Content, Text, Card, CardItem, Picker, Item, Input, Label, Button } from 'native-base';
import { StyleSheet, View, FlatList,TouchableOpacity,Image, Alert } from 'react-native';
import SelectMultiple from 'react-native-select-multiple';
import ActionSheet from '@yfuks/react-native-action-sheet';
// https://github.com/tableflip/react-native-select-multiple

var COUNTRYLIST = ["** Select Country **","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania","Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","St. Lucia","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

var STATELIST = ["** Select State **","Johor", "Kedah", "Kelantan", "Kuala Lumpur", "Labuan", "Malacca", "Negeri Sembilan", "Pahang", "Perak", "Perlis", "Penang", "Sabah", "Sarawak", "Selangor", "Terengganu"];

var ORGANISATION =[
    '** SELECT ORGANISATION **',
    'LOCAL COMPANY',
    'INDIVIDUAL',
    'SME',
    'MNC',
    'ORGANIZATION/ASSOCIATION',
    'COMPANY REGISTERED UNDER STATE REGENCY',
    'GOVERNMENT',
    'OTHERS (ORGANISATION / SOCIETIES / INTERNATIONAL)',
    'GLC',
    'FOREIGN COMPANY',
    'KOPERASI',
    'PROFESSIONAL & TECHNICAL SERVICES'
];

var BUSINESS = ['Manufacturer', 'Retailer', 'Exporter','Importer','Government / Authority','Distributor / Wholesaler','Academic','Association / NGO','Others (Please specify below)'];

var CANCEL_COUNTRY = 0;
var CANCEL_STATE = 0;
var CANCEL_ORG = 0;

class CourseRegisterScreen extends React.Component {


    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenName', 'Registration'),
            headerStyle:{
                backgroundColor: '#2C3B8D',
            },
            headerTitleStyle:{
                fontWeight: 'bold',
                color: 'white',
                alignSelf: 'center',
                fontSize: 20,
                flex: 2,
            }
        };
    };

    constructor() {
        super();
        this1 = this;
        this.state = {
            itemSelected: 'init',
            optionaltext : 'Company Registration No (ROC) ',
            selectedId: 0,
            selectedBus: [],

            selectedCountry:'Malaysia',
            selectedCountryCode:'037',
            selectedState:'Johor',
            selectedStateCode:'004',
            selectedOrg : 'LOCAL COMPANY',

            fieldPerName:'',
            fieldPerRegNo:'',
            fieldPerGSTID:'',
            fieldPerCompanyName:'',
            fieldPerEmail:'',
            fieldPerAdd1:'',
            fieldPerAdd2:'',
            fieldPerAdd3:'',
            fieldPerMobile:'',
            fieldPerOfficeNo:'',
            fieldPerFaxNo:'',

            selectedNature:''
        }

    }

    componentDidMount() {
      const { navigation } = this.props;
      const fees = navigation.getParam('fees');
      const titleid = navigation.getParam('titleid');
      const dateid = navigation.getParam('dateid');

      console.log('PASSED DATA');
      console.log('FEES :'+fees);
      console.log('TITLE ID :'+titleid);
      console.log('DATE ID :'+dateid);
    }


    onValueChange(value){

    }

      onCountryChange(value){

      }

      showCountrySheet = () => {
        ActionSheet.showActionSheetWithOptions({
          options: COUNTRYLIST,
          cancelButtonIndex: CANCEL_COUNTRY,
          tintColor: 'blue'
        },
        (buttonIndex) => {
          if(!buttonIndex){
          }else{
            this.setState({ selectedCountry: COUNTRYLIST[buttonIndex] });

            value = COUNTRYLIST[buttonIndex];

            if(value === 'Malaysia'){
              this.setState({selectedCountryCode:'037'},this.triggerCountry);
            }else{
              this.setState({selectedCountryCode:'000'},this.triggerCountry);
            }

          }
        });
      };

      showStateSheet = () => {
        ActionSheet.showActionSheetWithOptions({
          options: STATELIST,
          cancelButtonIndex: CANCEL_STATE,
          tintColor: 'blue'
        },
        (buttonIndex) => {
          if(!buttonIndex){
          }else{
            this.setState({ selectedState: STATELIST[buttonIndex] });

            value = STATELIST[buttonIndex];

            if(value === 'Johor'){
              this.setState({selectedStateCode:'004'},this.triggerState);
            }else if(value === 'Kedah'){
              this.setState({selectedStateCode:'010'},this.triggerState);
            }else if(value === 'Kelantan'){
              this.setState({selectedStateCode:'008'},this.triggerState);
            }else if(value === 'Kuala Lumpur'){
              this.setState({selectedStateCode:'002'},this.triggerState);
            }else if(value === 'Labuan'){
              this.setState({selectedStateCode:'016'},this.triggerState);
            }else if(value === 'Malacca'){
              this.setState({selectedStateCode:'013'},this.triggerState);
            }else if(value === 'Negeri Sembilan'){
              this.setState({selectedStateCode:'003'},this.triggerState);
            }else if(value === 'Pahang'){
              this.setState({selectedStateCode:'006'},this.triggerState);
            }else if(value === 'Perak'){
              this.setState({selectedStateCode:'012'},this.triggerState);
            }else if(value === 'Perlis'){
              this.setState({selectedStateCode:'009'},this.triggerState);
            }else if(value === 'Penang'){
              this.setState({selectedStateCode:'011'},this.triggerState);
            }else if(value === 'Sabah'){
              this.setState({selectedStateCode:'014'},this.triggerState);
            }else if(value === 'Sarawak'){
              this.setState({selectedStateCode:'015'},this.triggerState);
            }else if(value === 'Selangor'){
              this.setState({selectedStateCode:'001'},this.triggerState);
            }else if(value === 'Terengganu'){
              this.setState({selectedStateCode:'007'},this.triggerState);
            }

          }
        });
      };

      showOrgSheet = () => {
        ActionSheet.showActionSheetWithOptions({
          options: ORGANISATION,
          cancelButtonIndex: CANCEL_ORG,
          tintColor: 'blue'
        },
        (buttonIndex) => {
          if(!buttonIndex){
          }else{
            this.setState({ selectedOrg: ORGANISATION[buttonIndex] });

            if(buttonIndex == 1)
            {
                this.setState({optionaltext : 'Company Registration No (ROC)' })
            }
            else if(buttonIndex == 2){
                this.setState({optionaltext : 'NRIC No' });
            }
            else if(buttonIndex == 7){
                this.setState({optionaltext : 'Leave Blank' });
            }
            else{
                this.setState({optionaltext : 'Registration No' });
            }
          }
        });
      };


    triggerCountry(){
      console.log(this.state.selectedCountryCode);
    }

    triggerState(){
      console.log(this.state.selectedStateCode);
    }

    onCheckBoxPress(value) {
        this.setState({
          selectedId: value
        });
      }


    onSelectionsChange = (selectedBus) => {
        // selectedBus is array of { label, value }
        this.setState({ selectedBus },this.busFilter);
        var result = selectedBus.map(e => e.value).join(",");
        this.setState({ selectedNature:result });
        console.log(result);
      }

      busFilter(){
        var arrArray = this1.state.selectedBus;
        console.log(arrArray);
      }

      goNext(){
        const { navigation } = this.props;
        const courseName = navigation.getParam('screenName', 'COURSE NAME');
        const fees = navigation.getParam('fees');
        const titleid = navigation.getParam('titleid');

        const fPerName = this.state.fieldPerName;
        const fPerCompanyName = this.state.fieldPerCompanyName;
        const fPerEmail = this.state.fieldPerEmail;
        const fPerAdd1 = this.state.fieldPerAdd1;
        const fPerNumber = this.state.fieldPerMobile;

        if(fPerName === ''){
          Alert.alert('Contact Person Name is required.');
        }else if(fPerCompanyName === ''){
          Alert.alert('Company Name is required.');
        }else if(fPerEmail === ''){
          Alert.alert('Email is required.');
        }else if(fPerAdd1 === ''){
          Alert.alert('Address 1 is required.');
        }else if(fPerNumber === ''){
          Alert.alert('Contact Number is required.');
        }else{
          this.props.navigation.navigate('CourseRegisterPart',{
                  screenName: 'Registration',
                  courseName:courseName,
                  fees: fees,
                  titleid: titleid,
                  dateid: this.props.navigation.state.params.dateid,

                  pername: this.state.fieldPerName,
                  perorgtype: this.state.selectedOrg,
                  perregno: this.state.fieldPerRegNo,
                  percompgstid: this.state.fieldPerGSTID,
                  percompname: this.state.fieldPerCompanyName,
                  peremail: this.state.fieldPerEmail,
                  peradd1: this.state.fieldPerAdd1,
                  peradd2: this.state.fieldPerAdd2,
                  peradd3: this.state.fieldPerAdd3,
                  percountry: this.state.selectedCountryCode,
                  perstate: this.state.selectedStateCode,
                  permobile: this.state.fieldPerMobile,
                  peroffice: this.state.fieldPerOfficeNo,
                  perfax: this.state.fieldPerFaxNo,
                  pernature: this.state.selectedNature
                }
              );
        }
      }

    render() {
        console.disableYellowBox = true;
        const { navigation } = this.props;
        return (
        <Container>
            <Content padder>
            <View>
                <Text style={styles.maintitle }>A. PERSONAL INFORMATION</Text>
            </View>

                <View style = {{ marginTop:10 }}>
                    <Card>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection: 'column' }}>
                                <Item stackedLabel>
                                    <Label style = {styles.formtitle}>Contact Person Name *</Label>
                                    <Input
                                    onChangeText={(fieldPerName) => this.setState({fieldPerName})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>
                        </CardItem>
                        {/* <View style = {{ flex:1, flexDirection:'column' }}>
                            <Text>{this.state.selected}</Text>
                        </View> */}
                    </Card>
                    <Card>
                        <CardItem >
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Text style = {styles.formtitle}>Organisation Type *</Text>

                                <TouchableOpacity onPress={this.showOrgSheet} style={styles.wrapStateSelect}>
                                  <Text style={styles.wSSLeft}>
                                    {this.state.selectedOrg}
                                  </Text>
                                  <Image source={require('./img/sort-down.png')} style={styles.wSSRight}/>
                                </TouchableOpacity>

                            </View>

                        </CardItem>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Item stackedLabel>
                                    <Label style = {styles.formtitle}>{ this.state.optionaltext }</Label>
                                    <Input
                                    onChangeText={(fieldPerRegNo) => this.setState({fieldPerRegNo})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Item stackedLabel>
                                    <Label style = {styles.formtitle}>Company GST ID</Label>
                                    <Input
                                    onChangeText={(fieldPerGSTID) => this.setState({fieldPerGSTID})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Item stackedLabel>
                                    <Label style = {styles.formtitle}>Company Name *</Label>
                                    <Input
                                    onChangeText={(fieldPerCompanyName) => this.setState({fieldPerCompanyName})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Item stackedLabel>
                                    <Label style = {styles.formtitle}>Email *</Label>
                                    <Input
                                    onChangeText={(fieldPerEmail) => this.setState({fieldPerEmail})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>

                        </CardItem>
                        <CardItem>
                            <View>
                                <Text style = {styles.box}>Your email address will be your registration ID.You are not allowed to change or update your email ID once your registration are submitted.</Text>
                            </View>
                        </CardItem>

                    </Card>
                    <Card>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Text style = {styles.formtitle}>Address *</Text>
                                <Item stackedLabel>
                                    <Label>Address Line 1</Label>
                                    <Input
                                    onChangeText={(fieldPerAdd1) => this.setState({fieldPerAdd1})}
                                    style = {styles.inputfix}/>
                                </Item>
                                <Item stackedLabel>
                                    <Label>Address Line 2</Label>
                                    <Input
                                    onChangeText={(fieldPerAdd2) => this.setState({fieldPerAdd2})}
                                    style = {styles.inputfix}/>
                                </Item>
                                <Item stackedLabel>
                                    <Label>Address Line 3</Label>
                                    <Input
                                    onChangeText={(fieldPerAdd3) => this.setState({fieldPerAdd3})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>

                        </CardItem>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Text style = {styles.formtitle}>Country *</Text>
                                <TouchableOpacity onPress={this.showCountrySheet} style={styles.wrapStateSelect}>
                                  <Text style={styles.wSSLeft}>
                                    {this.state.selectedCountry.toUpperCase()}
                                  </Text>
                                  <Image source={require('./img/sort-down.png')} style={styles.wSSRight}/>
                                </TouchableOpacity>
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Text style = {styles.formtitle}>State *</Text>
                                <TouchableOpacity onPress={this.showStateSheet} style={styles.wrapStateSelect}>
                                  <Text style={styles.wSSLeft}>
                                    {this.state.selectedState.toUpperCase()}
                                  </Text>
                                  <Image source={require('./img/sort-down.png')} style={styles.wSSRight}/>
                                </TouchableOpacity>
                            </View>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Text style = {styles.formtitle}>Contact No. </Text>
                                <Item stackedLabel>
                                    <Label>Mobile *</Label>
                                    <Input
                                    onChangeText={(fieldPerMobile) => this.setState({fieldPerMobile})}
                                    style = {styles.inputfix}/>
                                </Item>
                                <Item stackedLabel>
                                    <Label>Office</Label>
                                    <Input
                                    onChangeText={(fieldPerOfficeNo) => this.setState({fieldPerOfficeNo})}
                                    style = {styles.inputfix}/>
                                </Item>
                                <Item stackedLabel>
                                    <Label>Fax</Label>
                                    <Input
                                    onChangeText={(fieldPerFaxNo) => this.setState({fieldPerFaxNo})}
                                    style = {styles.inputfix}/>
                                </Item>
                            </View>
                        </CardItem>
                    </Card>
                    <Card>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column'}}>
                                <Text style = {styles.formtitle}>Nature of Business </Text>
                                <SelectMultiple
                                    single
                                    style = {styles.select}
                                    items={BUSINESS}
                                    selectedItems={this.state.selectedBus}
                                    onSelectionsChange={this.onSelectionsChange} />
                            </View>
                        </CardItem>
                        <CardItem>
                            <View style = {{ flex:1, flexDirection:'column' }}>
                                <Item general>
                                    <Input style = {styles.inputfix}/>
                                </Item>
                            </View>
                        </CardItem>
                    </Card>

                    <Card transparent>
                        <Button block onPress={this.goNext.bind(this)}>
                            <Text>Next</Text>
                        </Button>
                    </Card>

                </View>
            </Content>
        </Container>
        );
    }
}



const styles = StyleSheet.create({
    select:{

    },
    title:{
        marginBottom:7,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        padding:20,
    },
    maintitle:{
        fontSize:20,
        fontWeight:'bold',
        color: '#fff',
        backgroundColor:'#6075BA',
        borderRadius: 5,
        padding:10,
    },
    detail:{
        fontSize:20,
    },
    text:{
        marginBottom:5,
    },
    formtext:{
        fontSize:20,
    },
    box:{
        borderColor:'#2C3B8D',
        borderWidth:2,
        padding:10,
        borderRadius:5,


    },
    formheader:{
        borderRadius:0,
        backgroundColor: '#2C3B8D',
        color:'white',
    },
    formtitle:{
        fontWeight: 'bold',
        marginBottom:0,
        color: '#F7912D',
    },
    inputfix:{
        marginBottom: 0,
        paddingBottom: 0,
        marginTop:3,
        paddingLeft:0,
    },
    wrapStateSelect:{
      flexDirection:'row',
      width:'100%',
      height:44,
      alignItems:'center'
    },
    wSSRight:{
      width:9,
      height:9,
      position:'absolute',
      right:0
    }
});

export default CourseRegisterScreen;
