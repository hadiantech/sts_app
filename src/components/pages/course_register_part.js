import React, {Component} from 'react';
import { Container, Content, Text, Card, CardItem, Item, Input, Label, Button } from 'native-base';
import { Modal, Alert, StyleSheet, View, TextInput, TouchableOpacity, FlatList, FlatListItem, Image } from 'react-native';
import ActionSheet from '@yfuks/react-native-action-sheet';
import { NavigationActions,StackActions } from 'react-navigation';
import Spinner from 'react-native-loading-spinner-overlay';

var PAYMENTMETHOD =[
    '** PAYMENT METHOD **',
    'BANK IN CASH',
    'ELECTRONIC BANK TRANSFER(EFT)',
    'CHEQUE',
    'LOCAL ORDER(GOV/AGENCIES ONLY)'
];

var CANCEL_PAYMENT = 0;

class CourseRegisterPartScreen extends React.Component {


    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenName', 'Registration'),
            headerStyle:{
                backgroundColor: '#2C3B8D',
            },
            headerTitleStyle:{
                fontWeight: 'bold',
                color: 'white',
                alignSelf: 'center',
                fontSize: 20,
                flex: 2,
            }
        };
    };

    constructor(props) {
        super(props);
        const participants = [
            { key: 0, 'name': 'Ahmad Kamarul' },
            { key: 0, 'name': 'Benedict Bally' },
            { key: 0, 'name': 'Cyntia Eng' },
            { key: 0, 'name': 'Dermawan Hisham' },
            { key: 0, 'name': 'Earnest Felix' },
          ];
        this.state = {
            visible: false,
            participants: participants,
            modalVisible: false,
            newName: '',
            arrayOfParticipants: [],
            fieldParName:'',
            fieldParPosition:'',
            fieldParEmail:'',
            fieldParMobile:'',

            finalDiscount:'0',
            finalPrice:'0',

            selectedPayment:'** PAYMENT METHOD **'
        }

    }

    componentDidMount() {
      const { navigation } = this.props;
      const fees = navigation.getParam('fees');
      const titleid = navigation.getParam('titleid');
      const dateid = navigation.getParam('dateid');
      const pername = navigation.getParam('pername');
      const perorgtype = navigation.getParam('perorgtype');
      const perregno = navigation.getParam('perregno');
      const percompgstid = navigation.getParam('percompgstid');
      const percompname = navigation.getParam('percompname');
      const peremail = navigation.getParam('peremail');
      const peradd1 = navigation.getParam('peradd1');
      const peradd2 = navigation.getParam('peradd2');
      const peradd3 = navigation.getParam('peradd3');
      const percountry = navigation.getParam('percountry');
      const perstate = navigation.getParam('perstate');
      const permobile = navigation.getParam('permobile');
      const peroffice = navigation.getParam('peroffice');
      const perfax = navigation.getParam('perfax');
      const pernature = navigation.getParam('pernature');

      console.log('+++++++++++++++++++++++++++++');
      console.log('PASSED DATA 2');
      console.log('FEES :'+fees);
      console.log('TITLE ID :'+titleid);
      console.log('DATE ID :'+dateid);
      console.log('PERSONAL NAME :'+pername);
      console.log('ORGANISATION TYPE :'+perorgtype);
      console.log('REGISTRATION NUMBER :'+perregno);
      console.log('COMPANY GST ID :'+percompgstid);
      console.log('COMPANY NAME :'+percompname);
      console.log('PERSONAL EMAIL :'+peremail);
      console.log('PERSONAL ADDRESS 1 :'+peradd1);
      console.log('PERSONAL ADDRESS 2 :'+peradd2);
      console.log('PERSONAL ADDRESS 3 :'+peradd3);
      console.log('COUNTRY CODE :'+percountry);
      console.log('STATE CODE :'+perstate);
      console.log('PERSONAL MOBILE :'+permobile);
      console.log('COMPANY PHONE :'+peroffice);
      console.log('COMPANY FAX :'+perfax);
      console.log('NATURE OF BUSINESS :'+pernature);
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    addParticipant(){
      let fetchedParticipant = this.state.arrayOfParticipants;

      let parName = this.state.fieldParName;
      let parPosition = this.state.fieldParPosition;
      let parEmail = this.state.fieldParEmail;
      let parMobile = this.state.fieldParMobile;


      if(parName === ''){
        Alert.alert('Participant name is required.');
      }else if(parPosition === ''){
        Alert.alert('Participant position is required.');
      }else if(parEmail === ''){
        Alert.alert('Participant email is required.');
      }else if(parMobile === ''){
        Alert.alert('Participant mobile number is required.');
      }else{
        const found = fetchedParticipant.some(item => item.email === parEmail);

        if (found) {
          Alert.alert('You cannot add same participant.');
        }else{
          var partiJson = { "name":parName, "position":parPosition, "email":parEmail, "mobileno":parMobile };
          this.setState({
            arrayOfParticipants: [...fetchedParticipant, partiJson]
          },
          this.updatedParti
          );
        }
      }

    }

    deleteParti(index){

      Alert.alert(
'Delete Participant',
'Are you sure want to delete this participant?',
[
  {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
  {text: 'OK', onPress: () => this.confirmDelete(index)},
]
)


    }

    confirmDelete(index){

      const filteredArray = this.state.arrayOfParticipants.filter((_, i) => i !== index);
        this.setState({
          arrayOfParticipants: filteredArray
        },this.alterDelete);
    }

    alterDelete(){

      var arrPar = this.state.arrayOfParticipants;

      const { navigation } = this.props;
      const fees = navigation.getParam('fees');

      var priceCal = fees * arrPar.length;

      const fPrice = this.state.finalPrice;
      const subStract = fPrice - fees;


      if(arrPar.length < 3){
        this.setState({
          finalPrice: priceCal,
          finalDiscount: 0
        });
      }else if(arrPar.length > 2 && arrPar.length < 5){

        var divideFirst = priceCal * 0.05;
        var filterPrice = priceCal - divideFirst;

        this.setState({
          finalPrice: filterPrice,
          finalDiscount: 5
        });
      }else if(arrPar.length > 4){

        var divideFirst = priceCal * 0.10;
        var filterPrice = priceCal - divideFirst;

        this.setState({
          finalPrice: filterPrice,
          finalDiscount: 10
        });
      }
    }

    updatedParti(){
      this.setState({
        fieldParName:'',
        fieldParPosition:'',
        fieldParEmail:'',
        fieldParMobile:''
      });
      const { navigation } = this.props;
      const fees = navigation.getParam('fees');

      var arrPar = this.state.arrayOfParticipants;

      var priceCal = fees * arrPar.length;

      console.log(this.state.arrayOfParticipants);

      if(arrPar.length > 2 && arrPar.length < 5){

        var divideFirst = priceCal * 0.05;
        var filterPrice = priceCal - divideFirst;
        this.setState({
          finalPrice: filterPrice,
          finalDiscount: 5
        });
      }else if(arrPar.length > 4){
        var divideFirst = priceCal * 0.10;
        var filterPrice = priceCal - divideFirst;
        this.setState({
          finalPrice: filterPrice,
          finalDiscount: 10
        });
      }else{
        this.setState({
          finalPrice: priceCal,
          finalDiscount: 0
        });
      }

      this.setModalVisible(!this.state.modalVisible);
    }
    // addParticipant() {
    //     const participants = this.state.participants.slice();
    //     participants.unshift({ key: 1,  'name': this.state.input });
    //     this.setState({ participants: participants});
    //     this.setModalVisible(!this.state.modalVisible);
    // }

    showPaymentSheet = () => {
      ActionSheet.showActionSheetWithOptions({
        options: PAYMENTMETHOD,
        cancelButtonIndex: CANCEL_PAYMENT,
        tintColor: 'blue'
      },
      (buttonIndex) => {
        if(!buttonIndex){
        }else{
          this.setState({ selectedPayment: PAYMENTMETHOD[buttonIndex] });
        }
      });
    };

    submitForm(){
      var arrPar = this.state.arrayOfParticipants;
      var payMethod = this.state.selectedPayment;

      if(arrPar.length == 0){
        Alert.alert('Unable to submit. Please add at least 1 participant.');
      }else if(payMethod === '** PAYMENT METHOD **'){
        Alert.alert('Please select payment method.');
      }else{

        this.setState({
            visible: true
        });

        const { navigation } = this.props;
        const fees = navigation.getParam('fees');
        const titleid = navigation.getParam('titleid');
        const dateid = navigation.getParam('dateid');
        const pername = navigation.getParam('pername');
        const perorgtype = navigation.getParam('perorgtype');
        const perregno = navigation.getParam('perregno');
        const percompgstid = navigation.getParam('percompgstid');
        const percompname = navigation.getParam('percompname');
        const peremail = navigation.getParam('peremail');
        const peradd1 = navigation.getParam('peradd1');
        const peradd2 = navigation.getParam('peradd2');
        const peradd3 = navigation.getParam('peradd3');
        const percountry = navigation.getParam('percountry');
        const perstate = navigation.getParam('perstate');
        const permobile = navigation.getParam('permobile');
        const peroffice = navigation.getParam('peroffice');
        const perfax = navigation.getParam('perfax');
        const pernature = navigation.getParam('pernature');
        const perpostcode = '';
        const percity = '';
        const permobilepre = '';
        const percoor = 'MURNI';

        console.log('https://www.sirimsense.com/sstsapi/registerInfo.php?contname='+pername +'&comptype='+perorgtype+'&compregno='+perregno+'&compname='+percompname+'&contemail='+peremail+'&address1='+peradd1+'&address2='+peradd2+'&address3='+peradd3+'&postcode='+perpostcode+'&city='+percity+'&state='+perstate+'&country='+percountry+'&mobileprefix='+permobilepre+'&mobile='+permobile+'&phone='+peroffice+'&fax='+perfax+'&rdPayMode='+payMethod+'&bussothers='+pernature+'&coordinator='+percoor+'&amount='+this.state.finalPrice+'&discount='+this.state.finalDiscount+'&compgstid='+percompgstid+'&chcknotifysms=Y'+'&titleid='+titleid+'&dateid='+dateid);

        fetch('https://www.sirimsense.com/sstsapi/registerInfo.php?contname='+pername +'&comptype='+perorgtype+'&compregno='+perregno+'&compname='+percompname+'&contemail='+peremail+'&address1='+peradd1+'&address2='+peradd2+'&address3='+peradd3+'&postcode='+perpostcode+'&city='+percity+'&state='+perstate+'&country='+percountry+'&mobileprefix='+permobilepre+'&mobile='+permobile+'&phone='+peroffice+'&fax='+perfax+'&rdPayMode='+payMethod+'&bussothers='+pernature+'&coordinator='+percoor+'&amount='+this.state.finalPrice+'&discount='+this.state.finalDiscount+'&compgstid='+percompgstid+'&chcknotifysms=Y'+'&titleid='+titleid+'&dateid='+dateid,{
            method: 'GET'
        })
        .then((response) => response.json())

        .then((responseJson) =>
        {
          const data = responseJson[0];
          const dataregid = data.regid;
          console.log(data);
          console.log(data.regid);

          var jj = 0;
          for (var i in arrPar) {
            jj++
            console.log('https://www.sirimsense.com/sstsapi/registerPax.php?regid='+dataregid +'&titleid='+titleid+'&dateid='+dateid+'&paxname='+arrPar[i].name+'&paxposi='+arrPar[i].position+'&paxemail='+arrPar[i].email+'&paxmobile='+arrPar[i].mobileno+'&paxfees='+fees);

            fetch('https://www.sirimsense.com/sstsapi/registerPax.php?regid='+dataregid +'&titleid='+titleid+'&dateid='+dateid+'&paxname='+arrPar[i].name+'&paxposi='+arrPar[i].position+'&paxemail='+arrPar[i].email+'&paxmobile='+arrPar[i].mobileno+'&paxfees='+fees,{
                method: 'GET'
            })
            .then((response) => response.json())

            .then((responseJson) =>
            {
              console.log('J value is:'+jj);
              console.log('lengt arr value is:'+ arrPar.length);
              if(jj == arrPar.length){
                console.log(responseJson);

                this.setState({
                    visible: true
                });

                setTimeout(() => {
Alert.alert('Your form has been submitted!')
}, 100);

                const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
        });
    this.props.navigation.dispatch(resetAction);


              }
            })
            .catch((error) =>
            {
                console.error(error);
            });

          }

        })
        .catch((error) =>
        {
            console.error(error);
        });


      }
    }

    render() {
        console.disableYellowBox = true;
        const { navigation } = this.props;
        const fees = navigation.getParam('fees');

        return (

        <Container>
            <View>

            <Spinner visible={this.state.visible} />

                <Modal
                    style={{ padding:20 }}
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                    <View style={{marginTop: 22, padding:15}}>
                        <Text style = {styles.formtitle}>Please fill in the fields below to register participant for this event.</Text>
                        <Item stackedLabel>
                            <Label>Name *</Label>
                            <Input
                            onChangeText={(fieldParName) => this.setState({fieldParName})}
                            style = {styles.inputfix}/>
                        </Item>
                        <Item stackedLabel>
                            <Label>Position *</Label>
                            <Input
                            onChangeText={(fieldParPosition) => this.setState({fieldParPosition})}
                            style = {styles.inputfix}/>
                        </Item>
                        <Item stackedLabel>
                            <Label>Email *</Label>
                            <Input
                            onChangeText={(fieldParEmail) => this.setState({fieldParEmail})}
                            style = {styles.inputfix}/>
                        </Item>
                        <Item stackedLabel>
                            <Label>Mobile No *</Label>
                            <Input
                            onChangeText={(fieldParMobile) => this.setState({fieldParMobile})}
                            style = {styles.inputfix}/>
                        </Item>
                    </View>

                    <View style = {{ flex:1, padding:15 }}>
                        <Button block onPress={this.addParticipant.bind(this)} >
                            <Text>Add</Text>
                        </Button>
                        <Button bordered block danger onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{ marginTop:15 }}>
                            <Text>Cancel</Text>
                        </Button>
                    </View>
                </Modal>
            </View>
            <Content padder>
                <View>
                    <Text style={styles.maintitle }>B. PARTICIPANT INFORMATION</Text>
                </View>

                <View style = {{ marginTop:10 }}>
                    <Card>
                        <CardItem>
                            <View style = {{ flex:1 }}>
                                <Button block info onPress={() => {this.setModalVisible(true);}}>
                                    <Text>Add New Participant</Text>
                                </Button>
                            </View>
                        </CardItem>
                    </Card>

                    <FlatList
                        data={this.state.arrayOfParticipants}
                        renderItem={({item, index}) => {
                        return (
                            <TouchableOpacity onPress={this.deleteParti.bind(this, index)}>
                                <Card>
                                    <CardItem>
                                        <View style={styles.partiRow}>
                                            <Text style={styles.partiRowName}>{ item.name }</Text>
                                            <Text style={styles.partiRowFees}>RM {fees}</Text>
                                        </View>
                                    </CardItem>
                                </Card>
                            </TouchableOpacity>
                        )
                        }}/>
                </View>

                <View style={styles.wrapPrice}>
                    <View style={styles.wrapDiscount}>
                      <Text style={styles.wDTTitle}>DISCOUNT (%)</Text>
                      <Text style={styles.wDTContent}>{this.state.finalDiscount}</Text>
                    </View>
                    <View style={styles.wrapTotalPrice}>
                      <Text style={styles.wDTTitle}>TOTAL (RM)</Text>
                      <Text style={styles.wDTContent}>{this.state.finalPrice}</Text>
                    </View>
                </View>

                <TouchableOpacity onPress={this.showPaymentSheet} style={styles.wrapStateSelect}>
                  <Text style={styles.wSSLeft}>
                    {this.state.selectedPayment.toUpperCase()}
                  </Text>
                  <Image source={require('./img/sort-down.png')} style={styles.wSSRight}/>
                </TouchableOpacity>


                <View style={styles.wrapSubmit}>
                    <Button block primary onPress={this.submitForm.bind(this)} ><Text>Submit</Text></Button>
                </View>
            </Content>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
  wrapStateSelect:{
    paddingLeft:10,
    paddingRight:10,
    marginTop:15,
    marginBottom:15,
    flexDirection:'row',
    width:'100%',
    height:44,
    alignItems:'center',
    borderWidth:1,
    borderColor:'#d1d1d1'
  },
  wSSRight:{
    width:9,
    height:9,
    position:'absolute',
    right:10
  },
  wrapPrice:{
    marginTop:5,
    flexDirection:'row',
    alignSelf:'stretch',
  },
  wrapDiscount:{
    alignItems:'center',
    justifyContent:'center',
    width:'50%',
    height:40,
    backgroundColor:'#ffffd8'
  },
  wrapTotalPrice:{
    alignItems:'center',
    justifyContent:'center',
    width:'50%',
    height:40,
    backgroundColor:'#ffffd8'
  },
  wDTTitle:{
    fontSize:13
  },
  wDTContent:{
    color:'#001c7a',
    fontSize:15
  },
    title:{
        marginBottom:7,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        padding:20,
    },
    maintitle:{
        fontSize:20,
        fontWeight:'bold',
        color: '#fff',
        backgroundColor:'#6075BA',
        borderRadius: 5,
        padding:10,
    },
    detail:{
        fontSize:20,
    },
    text:{
        marginBottom:5,
    },
    formtext:{
        fontSize:20,
    },
    box:{
        borderColor:'#2C3B8D',
        borderWidth:2,
        padding:10,
        borderRadius:5,
    },
    formheader:{
        borderRadius:0,
        backgroundColor: '#2C3B8D',
        color:'white',
    },
    formtitle:{
        fontWeight: 'bold',
        marginBottom:0,
        color: '#F7912D',
    },
    inputfix:{
        marginBottom: 0,
        paddingBottom: 0,
        marginTop:3,
        paddingLeft:0,
    },
    partiRow:{
      flexDirection:'row',
    },
    partiRowName:{
      width:'50%',
      textAlign:'left'
    },
    partiRowFees:{
      width:'50%',
      textAlign:'right'
    },
    wrapSubmit:{
      marginTop:10
    }
});

export default CourseRegisterPartScreen;
