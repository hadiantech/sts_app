import React, {Component} from 'react';
import {Content, Text} from 'native-base';

class AboutScreen extends React.Component {

    static navigationOptions = {
        headerTitle: "About",
    };

    constructor() {
        super();
    }

    render() {
        return (
        <Content padder>
            <Text>This is about</Text>
        </Content>
        );
    }
}

export default AboutScreen;