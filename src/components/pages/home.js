import React, { Component } from 'react';
import { Content, Text, Card, CardItem, Body, Container, View } from 'native-base';
import { StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

class HomeScreen extends Component {

    constructor(props){
        super(props);
        this.state ={
            visible: true,
            tempData: [
                {"catdesc":"STANDARDS BASED MANAGEMENT SYSTEMS","catid":"01", "img": require('./img/0.png'),"icon": require('./img/standard.png')},
                {"catdesc":"QUALITY & INNOVATION","catid":"02","img": require('./img/1.png'),"icon": require('./img/innovation.png')},
                {"catdesc":"CERTIFICATE & TECHNOLOGY PROGRAMMES","catid":"03","img": require('./img/2.png'),"icon": require('./img/certification.png')},
                {"catdesc":"LEAD AUDITOR COURSES","catid":"04","img": require('./img/3.png'),"icon": require('./img/lead.png')},
                {"catdesc":"MANAGEMENT COURSES","catid":"05","img": require('./img/4.png'),"icon": require('./img/management.png')},
                {"catdesc":"RECOGNITION SCHEMES","catid":"06","img": require('./img/5.png'),"icon": require('./img/recognition.png')},
                {"catdesc":"SEMINAR / WORKSHOP","catid":"07","img": require('./img/6.png'),"icon": require('./img/seminar.png')}
            ],
        }
    }

    componentDidMount(){
        return fetch('https://www.sirimsense.com/sstsapi/category.php?catstatusid=01')
          .then((response) => response.json())
          .then((responseJson) => {
            console.log(responseJson);
            this.setState({
                visible: false,
                dataSource: responseJson,
            }, function(){

            });

          })
          .catch((error) =>{
            console.error(error);
          });
    }

    static navigationOptions = {
        title: 'Home',
        headerStyle:{
            backgroundColor: '#2C3B8D',
        },
        headerTitleStyle:{
            fontWeight: 'bold',
            color: 'white',
            alignSelf: 'center',
            fontSize: 20,
            flex: 2,
        }
    };
    render() {
          console.disableYellowBox = true;

        return (
            <Container style = {{ backgroundColor: '#E1E1E1' }}>
                <Content style = {{ margin: 10 }}>

                    <Spinner visible={this.state.visible} />

                    <FlatList
                        data={this.state.tempData}
                        renderItem={({item, index}) =>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Courses',{
                            screenName: item.catdesc,
                            category_id: item.catid
                            })}>
                            <Card>
                                <CardItem style={styles.p0}>
                                    <Body>
                                        <View style={{ flex:1, alignSelf:'stretch', height:100, flexDirection: 'column' }}>
                                            <Image source={item.img} style = {{ alignSelf: 'stretch', resizeMode: 'cover', width:undefined, minHeight:40, maxHeight:100 }} />
                                        </View>
                                        <View style = {{ flex:1, flexDirection: 'row', padding:20, justifyContent: 'center', alignItems: 'center' }}>
                                            <View style  = {{ flex:1 }}>
                                                <Image source={item.icon} style = {{flex:1, width: 50, height: 50, resizeMode: 'contain', marginRight: 10 }} />
                                            </View>
                                            <View style = {{ flex: 3 }}>
                                                <Text style={ styles.blue }>{ item.catdesc }</Text>
                                            </View>
                                        </View>
                                    </Body>
                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    } />

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    p0:{
        paddingLeft:0,
        paddingRight:0,
        paddingBottom:0,
        paddingTop:0
    },
    bigblue: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 30,
    },
    blue:{
        color: '#2C3B8D'
    },
    bluebg:{
        backgroundColor: '#949FDE'
    },
    orange:{
        color: '#F7912D'
    },
    yellow:{
        color: '#F3B037'
    },
    black:{
        color: '#000'
    },
    white:{
        color: '#FFFFFF'
    },
    gray:{
        color: '#E1E1E1'
    },

});

export default HomeScreen;
