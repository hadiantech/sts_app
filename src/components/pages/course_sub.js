import React, { Component } from 'react';
import { Content, Text, Container } from 'native-base';
import { StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

export default class CourseSubScreen extends Component {

    constructor(props){
        super(props);
        this.state ={ 
            visible: true 
        }
    }

    componentDidMount(){
        const { navigation } = this.props;
        return fetch('https://www.sirimsense.com/sstsapi/title.php?catstatusid=01&year1=2018&crsid='+navigation.getParam('course_id', '01'))
            .then((response) => response.json())
            .then((responseJson) => {
            console.log(responseJson);
            this.setState({
                visible: false,
                dataSource: responseJson,
            }, function(){
                
            });

            })
            .catch((error) =>{
                console.error(error);
            });
    }

    static navigationOptions = {
        title: 'Select Sub-course',
        headerStyle:{
            backgroundColor: '#2C3B8D',
        },
        headerTitleStyle:{
            fontWeight: 'bold',
            color: 'white',
            alignSelf: 'center',
            fontSize: 20,
            flex: 2,
        }
    };

    render() {
        const { navigation } = this.props;
        const screenName = navigation.getParam('screenName', 'NO-ID');

        return (
            <Container style = {{ backgroundColor: '#E1E1E1' }}>
                <Content>
                    
                    <Spinner visible={this.state.visible} /> 
                    
                    {/* <Text>{JSON.stringify(screenName)}</Text> */}
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({item}) => 
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CourseDate',{
                            screenName: item.titledesc,
                            courseid: item.courseid,
                            fees: item.fees,
                            titledesc: item.titledesc,
                            titleid: item.titleid,
                            titleyear: item.titleyear,
                            })}>
                            <Text style={styles.item}>
                                {item.titledesc}
                            </Text>
                        </TouchableOpacity>
                    }/>
                </Content>
            </Container>
        );
    }
}

;

const styles = StyleSheet.create({
    p0:{
        paddingLeft:0,
        paddingRight:0,
        paddingBottom:0,
        paddingTop:0
    },
    bigblue: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 30,
    },
    blue:{
        color: '#2C3B8D'
    },
    bluebg:{
        backgroundColor: '#949FDE'
    },
    orange:{
        color: '#F7912D'
    },
    yellow:{
        color: '#F3B037'
    },
    black:{
        color: '#000'
    },
    white:{
        color: '#FFFFFF'
    },
    gray:{
        color: '#E1E1E1'
    },
    item:{
        backgroundColor: 'white',
        borderWidth:0.5,
        borderColor: '#E1E1E1',
        margin: 0,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop:20,
        paddingBottom:20,
    },

});

// export default DetailScreen;