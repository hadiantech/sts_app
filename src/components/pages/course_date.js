import React, {Component} from 'react';
import {Content, Text, Card, CardItem, Button } from 'native-base';
import { StyleSheet, FlatList, View, Alert } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';


class CourseDateScreen extends React.Component {

    constructor(props){
        super(props);
        this.state ={
            visible: true
        }
    }

    componentDidMount(){
        const { navigation } = this.props;
        return fetch('https://www.sirimsense.com/sstsapi/titledate.php?crsid='+navigation.getParam('courseid', '01')+'&titleid='+navigation.getParam('titleid', '01'))
            .then((response) => response.json())
            .then((responseJson) => {
            console.log(responseJson);
            this.setState({
                visible: false,
                dataSource: responseJson,
            }, function(){

            });

            })
            .catch((error) =>{
                console.error(error);
            });
    }

    componentWillMount(){
      console.log('willl mount');
    }

    static navigationOptions = {
        title: 'Select Sub-course',
        headerStyle:{
            backgroundColor: '#2C3B8D',
        },
        headerTitleStyle:{
            fontWeight: 'bold',
            color: 'white',
            alignSelf: 'center',
            fontSize: 20,
            flex: 2,
        }
    };

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('screenName', 'A Nested Details Screen'),
            headerStyle:{
                backgroundColor: '#2C3B8D',
            },
            headerTitleStyle:{
                fontWeight: 'bold',
                color: 'white',
                alignSelf: 'center',
                fontSize: 20,
                flex: 2,
            }
        };
    };

    state = {
        selectedDate: this.props.navigation.state.params.selectedDate,
        isSelected : false
    }

    updateDate = () => {
            this.setState({
                isSelected: this.props.navigation.state.params.date
            })
    }

    goNext(){
      const { navigation } = this.props;
      const fees = navigation.getParam('fees');
      const titleid = navigation.getParam('titleid');

      if(this.props.navigation.state.params.dateid == undefined){
        Alert.alert('Please select date first.');
      }else{
        this.props.navigation.navigate('CourseRegister',{
                screenName: 'Registration Form',
                courseName:this.props.navigation.state.params.screenName,
                date: this.props.navigation.state.params.date,
                fees: fees,
                titleid: titleid,
                dateid: this.props.navigation.state.params.dateid
              }
            );
      }


    }

    render() {
        console.disableYellowBox = true;
        const { navigation } = this.props;
        const screenName = navigation.getParam('screenName', 'NO-ID');

        return (
        <Content padder>

            <Spinner visible={this.state.visible} />

            <Card transparent>
                <CardItem>
                    <Content>
                        <Text style = {styles.title}>Course Name</Text>
                        <Text style = {styles.detail}>{ this.props.navigation.state.params.screenName }</Text>
                    </Content>
                </CardItem>
            </Card>

            <View style = {{ flex:1, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Card transparent style = {{ flex:1, flexBasis:50 }}>
                    <CardItem style = {{ flex:1, flexDirection:'column', alignItems:'flex-start' }}>
                        <Text style ={styles.title}>Fee</Text>
                        <Text style = {styles.detail}>RM { this.props.navigation.state.params.fees }</Text>
                    </CardItem>
                </Card>
                <Card transparent style = {{ flex:1, flexBasis:50 }}>
                    <CardItem style = {{ flex:1, flexDirection:'column', alignItems:'flex-start' }}>
                        <Text style ={styles.title}>Duration</Text>
                        <Text style = {styles.detail}>{ navigation.getParam('durationSelected', '0') } Day(s)</Text>
                    </CardItem>
                </Card>
            </View>
            <View>
                <Card transparent style = {{ flex:1, flexBasis:50 }}>
                    <CardItem style = {{ flex:1, flexDirection:'column', alignItems:'flex-start'}}>
                        <Text style ={styles.title}>Date & Location</Text>
                        <Text style = {styles.detail}>{ navigation.getParam('venueSelected', '') } </Text>
                        <Text style = {styles.detail}>{ navigation.getParam('dateStartSelected', '') } </Text>
                        <Button bordered style = {{ marginTop:10 }} info block onPress={() =>  this.props.navigation.navigate('CourseDateDetail',{
                            screenName: 'Select Date & Location',
                            listDate: this.state.dataSource
                            })}>
                            <Text>Select</Text>
                        </Button>
                    </CardItem>
                </Card>

                {/* <Card transparent style = {{ flex:1, flexBasis:50 }}>
                    <CardItem style = {{ flex:1, flexDirection:'column'}}>
                        <Text style ={styles.title}>Select Date & Location</Text>
                        <Form style = {{ marginBottom: 10, marginTop:-15 }}>
                            <Picker
                                style = {{ width:330 }}
                                selectedValue = {this.state.date} onValueChange = {this.updateDate}>
                                    <Picker.Item label = "Steve" value = "steve" />
                                    <Picker.Item label = "Ellen" value = "ellen" />
                                    <Picker.Item label = "Maria" value = "maria" />
                            </Picker>
                        </Form>
                    </CardItem>
                </Card> */}
            </View>

            <View>

            </View>

            <View>
                <Card transparent>
                    <Button block onPress={this.goNext.bind(this)}>
                        <Text>Next</Text>
                    </Button>
                </Card>
            </View>
        </Content>


        );
    }
}

const styles = StyleSheet.create({
    title:{
        marginBottom:7,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
    },
    detail:{
        fontSize:20,
    },
});

export default CourseDateScreen;
