import React, {Component} from 'react';

import {Linking, Alert, View} from 'react-native';

import {Content, Text} from 'native-base';

import QRCodeScanner from 'react-native-qrcode-scanner';
import { NavigationEvents, NavigationScreenProps } from 'react-navigation';

class QrScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            isFocused: undefined,
        };
    }

    static navigationOptions = {
        title: 'Scan',
        headerStyle:{
            backgroundColor: '#2C3B8D',
        },
        headerTitleStyle:{
            fontWeight: 'bold',
            color: 'white',
            alignSelf: 'center',
            fontSize: 20,
            flex: 2,
        }
    };

    onDidFocus = payload => {
        this.setState({ isFocused: true });
    };

    onDidBlur = payload => {
        this.setState({ isFocused: false });
    };

    onSuccess(e) {
        console.log('URL Scan',e.data);

        this.props.navigation.navigate('QRResult',{
           urlpassed: e.data,
         });
    // Linking
    //     .openURL(e.data)
    //     .catch(err => console.error('An error occured', err));
    }

    render() {
        const { isFocused } = this.state;

        return (
            <View>
            <NavigationEvents
                onDidFocus={this.onDidFocus}
                onDidBlur={this.onDidBlur}
            />
            {isFocused && (
                <QRCodeScanner
                onRead={this.onSuccess.bind(this)}
                fadeIn={false}
            />
            )}
            </View>

        );
    }
}

export default QrScreen;
