import React, {Component} from 'react';
import {Content, Text } from 'native-base';
import { StyleSheet, View, TouchableOpacity, WebView } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

class QRResultScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            visible: true
        }
    }

    componentDidMount(){
      const { navigation } = this.props;
      const urlget = navigation.getParam('urlpassed');
      console.log(urlget);
    }

    static navigationOptions = ({ navigation }) => {
        return {
            title: 'QR Details',
            headerStyle:{
                backgroundColor: '#2C3B8D',
            },
            headerTitleStyle:{
                fontWeight: 'bold',
                color: 'white',
                alignSelf: 'center',
                fontSize: 20,
                flex: 2,
            }
        };
    };

    loadEnd(){
      this.setState({visible: false});
    }

    render() {
        console.disableYellowBox = true;
        const { navigation } = this.props;
        const screenName = navigation.getParam('screenName', 'NO-ID');
        const urlget = navigation.getParam('urlpassed');

        return (
          <View style={styles.outWorld}>
          <Spinner visible={this.state.visible} />
        <WebView
        onLoadEnd={() => this.loadEnd()}
        source={{uri: urlget}}
        style={styles.container}
      />
      </View>


        );
    }
}

const styles = StyleSheet.create({
  outWorld:{
    flexGrow:1,
    backgroundColor:'#fff'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    margin:0
  },
    title:{
        marginBottom:7,
        fontWeight: 'bold',
        alignSelf: 'flex-start',
    }
});

export default QRResultScreen;
