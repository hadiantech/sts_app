import React, {Component} from 'react';
import {Footer, FooterTab, Button, Icon, Text} from 'native-base';
import {Actions} from 'react-native-router-flux';

export default class AppFooter extends Component {
    constructor() {
        super();
        this.state = {
            activeTabName: 'home'
        };
    }

    tabAction(tab) {
        this.setState({activeTabName: tab});
        if (tab === 'home') {
            Actions.home();
        } else if (tab === 'search') {
            Actions.search();
        }else {
            Actions.about();
        }
    }

    render() {
        return (
            <Footer>
                <FooterTab>
                    <Button active={(this.state.activeTabName === "home")? true: false} onPress={() => {this.tabAction('home')}}>
                        <Icon name="home"/>
                        <Text>Home</Text>
                    </Button>
                    <Button>
                        <Icon name="qr-scanner"/>
                        <Text>Scan</Text>
                    </Button>
                    <Button active={(this.state.activeTabName === "search")? true: false} onPress={() => {this.tabAction('search')}}>
                        <Icon name="search"/>
                        <Text>Search</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }
}
module.export = AppFooter;